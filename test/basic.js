NSCA = require('../index');

var nsca = new NSCA({
  port: 3000,             // port of NSCA agent
  host: '127.0.0.1',      // hostname of NSCA agent
  serviceHost: 'fooHost',    // this service's hostname.
  serviceName: 'fooService'
});

nsca.submit(
  NSCA.RESULT_OK,
  'Performance data',
  42,
  NSCA.UNIT_COUNTER
);
