module.exports = {
  RESULT_OK:       '0',
  RESULT_WARNING:  '1',
  RESULT_CRITICAL: '2',
  RESULT_UNKNOWN:  '3',

  UNIT_NUMBER:   undefined,
  UNIT_SECONDS:  's',
  UNIT_PERCENT:  '%',
  UNIT_BYTES:    'B',
  UNIT_KBYTES:   'KB',
  UNIT_MBYTES:   'MB',
  UNIT_COUNTER:  'c'
};
